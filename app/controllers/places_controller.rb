class PlacesController < ApplicationController
  before_action :set_place, only: [:show, :edit, :update, :destroy]
  require 'httparty'

  # GET /places
  # GET /places.json
  def index
    @places = Place.all
  end

  # GET /places/1
  # GET /places/1.json
  def show
    redirect_to "https://www.google.ro/maps/search/#{@place.Name}"
  end

  # GET /places/new
  def new
    @place = Place.new

  end

  # GET /places/1/edit
  def edit
  end

  # POST /places
  # POST /places.json
  def create
    @place = Place.new(place_params)
    response = HTTParty.get("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=#{@place.Name}&key=AIzaSyAGjxtGd68yzadXmRAe-u8ShoI7Isc_SuQ")
    parsed_response = JSON.parse(response.body)
    parsed_response['predictions'].each do |prediction|
      @place.Address = prediction['description']
      # get_photo_reference
      # puts(@image)
      break

    end
    puts(@name_of_place)
    respond_to do |format|
      if @place.save
        format.html { redirect_to @place, notice:"Place was successfully created with photo reference:}" }
        format.json { render :show, status: :created, location: @place }
      else
        format.html { render :new }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    respond_to do |format|
      if @place.update(place_params)
        format.html { redirect_to @place, notice: 'Place was successfully updated.' }
        format.json { render :show, status: :ok, location: @place }
      else
        format.html { render :edit }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to places_url, notice: 'Place was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

#   def get_photo_reference
#     photo_response = HTTParty.get("https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=#{@place.Name}&inputtype=textquery&fields=photos&key=AIzaSyAGjxtGd68yzadXmRAe-u8ShoI7Isc_SuQ")
#     photo_parsed_response = JSON.parse(photo_response.body)
#     photo_parsed_response['candidates'].each do |candidate|
#       candidate['photos'].each do |photo|
#         @image = photo['photo_reference']
#
#       end
#
#     end
#     @image
# end
  private
    # Use callbacks to share common setup or constraints between actions.
  def set_place
    @place = Place.find(params[:id])
  end

    # Never trust parameters from the scary internet, only allow the white list through.
  def place_params
    params.require(:place).permit(:Name, :Address, :@name_of_place)
  end
end
