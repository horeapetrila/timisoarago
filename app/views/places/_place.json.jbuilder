json.extract! place, :id, :Name, :Address, :created_at, :updated_at
json.url place_url(place, format: :json)
