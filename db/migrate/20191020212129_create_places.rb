class CreatePlaces < ActiveRecord::Migration[5.2]
  def change
    create_table :places do |t|
      t.string :Name
      t.string :Address

      t.timestamps
    end
  end
end
